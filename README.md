# Ddns

Ddns is a lightweight tool for updating dynamic DNS records.  Ddns tries to be
smart and only updates the IP address only if it has changed since the last time
it was ran.

Part of what makes this lightweight is its support for limited amount of dynamic
dns providers instead of trying to support all of them.  Currently it only
supports Google Domains which provides free dynamic dns for all domains managed
by them.  Adding support for other providers is possible if there is demand for
one.

## Installing

Installing requires that [Go 1](http://golang.org/doc/install).

```sh
$ go get github.com/vendion/ddns
```

## Running

Ddns is intended to be ran on a regular schedule, either via Cron or some cron
like service.  That being said it can be ran manually with

```sh
$ ddns
```

Ddns gets its configuration from a `.env` file in the directory it was ran in.
If this file is stored in another location or named differently an alternate
file can be used.

```sh
ddns -config=/path/to/.env
```

There is an example `env` file provided in the repo, this file defines three
environment variables used by Ddns: `DDNS_USER`, `DDNS_PASS`, `DDNS_HOST`.
These are used when updating the record on the dynamic dns provider.

The `.env` file looks like:

```sh
DDNS_USER=username
DDNS_PASS=password
DDNS_HOST=test.example.com
```

## License

Ddns is licensed under the 3 Clause BSD license.  For details on what this
means please see the LICENSE file provided with the source.
