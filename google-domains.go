package main

import (
	"fmt"
	"net/http"
	"net/url"
)

func googleDomainsUpdate(c config, IP string) error {
	URL, values := buildGoogleURL(c, IP)
	r, err := http.PostForm(URL, values)
	defer r.Body.Close()

	if err != nil {
		return fmt.Errorf("failed submitting current IP to Google Domains server: %v\n", err)
	}
	if r.StatusCode != 200 {
		return fmt.Errorf("server responded with %s\n", r.Status)
	}

	fmt.Println("IP successfully updated")

	return nil
}

func buildGoogleURL(c config, IP string) (string, url.Values) {
	userInfo := basicAuth(c)

	v := url.Values{}
	v.Set("hostname", c.hostname)
	v.Add("myip", IP)

	u, _ := url.Parse("https://domains.google.com/nic/update")
	u.User = userInfo

	return u.String(), v
}

func basicAuth(c config) *url.Userinfo {
	return url.UserPassword(c.username, c.password)
}
