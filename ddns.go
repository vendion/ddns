package main

import (
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/jpfuentes2/go-env"
)

var (
	cacheFile string
	cnf       *string
	cur       *bool
	last      *bool
)

type config struct {
	username, password, hostname string
}

func (c *config) parseConfig(envPath string) {
	// check if the environment variables are already set
	// if not load them from a .env file
	if os.Getenv("DDNS_USER") == "" ||
		os.Getenv("DDNS_PASS") == "" ||
		os.Getenv("DDNS_HOST") == "" {
		env.ReadEnv(envPath)
		env.ReadEnv(envPath + ".local")
	}

	c.username = os.Getenv("DDNS_USER")
	c.password = os.Getenv("DDNS_PASS")
	c.hostname = os.Getenv("DDNS_HOST")
}

type addr struct {
	IP   string
	time int64
}

func (a *addr) parseCachedFile() error {
	b, err := ioutil.ReadFile(cacheFile)
	if err != nil {
		return err
	}

	text := string(b)
	s := strings.Split(text, " ")
	t, err := strconv.ParseInt(s[0], 10, 64)
	if err != nil {
		// was unable to parse the date, assuming now
		t = time.Now().Unix()
	}
	a.time = t
	a.IP = s[1]

	return nil
}

func (a *addr) writeCacheFile() error {
	text := fmt.Sprintf("%d %s\n", a.time, a.IP)
	err := ioutil.WriteFile(cacheFile, []byte(text), 0644)
	return err
}

func (a *addr) printLastRun() string {
	if a.time == 0 {
		return "No last run information found"
	}

	t := time.Unix(a.time, 0)
	return fmt.Sprintf("Last run was: %s with IP: %s", t.Format("2006-01-02 03:04 pm"), a.IP)
}

func (a *addr) printCurrentAddress() string {
	return fmt.Sprintf("Current IP: %s", a.IP)
}

func init() {
	cnf = flag.String("config", "", "Path to the env file to load settings from")
	cur = flag.Bool("current", false, "Prints the current IP address")
	last = flag.Bool("last", false, "Prints the time Ddns last ran")
	flag.Parse()
}

func main() {
	if *cnf == "" {
		pwd, _ := os.Getwd()
		*cnf = path.Join(pwd, ".env")
	}

	var conf config
	conf.parseConfig(*cnf)

	tmpDir := os.TempDir()
	cacheFile = path.Join(tmpDir, "ddns")

	// check for cached IP information, if there is read it in
	var oldIP addr
	_, err := os.Stat(cacheFile)
	if err == nil {
		err = oldIP.parseCachedFile()
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error: %v\n", err)
		}
	}

	if *last {
		fmt.Println(oldIP.printLastRun())
		os.Exit(0)
	}

	// fetch current IP
	var newIP addr
	newIP.IP, err = getIPAddr()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %v\n", err)
		os.Exit(1)
	}
	newIP.time = time.Now().Unix()

	if *cur {
		fmt.Println(newIP.printCurrentAddress())
		os.Exit(0)
	}

	// ensure that we are not hacking time
	if oldIP.time < newIP.time {
		// check if updating the IP is needed
		if oldIP.IP == "" || oldIP.IP != newIP.IP {
			err := googleDomainsUpdate(conf, newIP.IP)
			if err != nil {
				fmt.Fprintf(os.Stderr, "Error: %v\n", err)
				os.Exit(1)
			}
		}
	}

	// log current time and IP
	fmt.Println("Current IP Address is", newIP.IP)
	newIP.writeCacheFile()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: could not write to temporary file: %v", err)
		os.Exit(1)
	}

	os.Exit(0)
}

func getIPAddr() (string, error) {
	r, err := http.Get("http://ipecho.net/plain")
	if err != nil || r.StatusCode != 200 {
		r, err = http.Get("http://checkip.dyndns.org/")
		if err != nil {
			return "", fmt.Errorf("failed to get WAN IP: %v", err)
		}
	}
	defer r.Body.Close()

	if r.StatusCode != 200 {
		return "", fmt.Errorf("server responded with %s", r.Status)
	}

	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return "", fmt.Errorf("failed to parse server response: %v", err)
	}

	ip := string(b)
	m, err := regexp.Match("Current IP Address: ", b)
	if err != nil {
		return "", errors.New("failed to compile regexp string, can not use for matching")
	}
	if m {
		s := strings.Split(ip, ":")
		ip = strings.TrimSpace(s[1])
	}

	return ip, nil
}
